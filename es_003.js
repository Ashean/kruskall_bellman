/*File js dedito alla gestione dei grafi*/

let arrayNode =[];
let arrayEdge = [];
let nodeCounter=0;
let edgeCounter=0;
var cy;
window.onload = function() {

   cy = cytoscape({
  container: document.getElementById('cy'), // container to render in
  // container: $('#cy'),
  elements: [
  ],
  style: [ // the stylesheet for the graph
    {
      selector: 'node',
      style: {
        'background-color': '#666',
        'label': 'data(id)'
      }
    },

    {
      selector: 'edge',
      style: {
        'width': 2,
        'line-color': '#ccc',
        'curve-style': 'bezier', // .. and target arrow works properly
        'target-arrow-color': '#ccc',
        'target-arrow-shape': 'triangle',
        'label':'data(id)'

      }
    }
  ],
  // see http://js.cytoscape.org/#layouts
  layout: {
    name: 'grid',
    rows: 2,
    cols: 4
  }
  });

  function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //Il max è incluso e il min è incluso
}

  function creaNode(p,strp){
   cy.add({
    group: 'nodes',
    data: { id:p },
    position: { x: getRandomIntInclusive(0,400), y: getRandomIntInclusive(0,400) }
  });
  let obj = Object.freeze({
    peso: p,
    name: strp
  });
  arrayNode[nodeCounter]=obj;
  nodeCounter++;
    return obj;
};

  function creaEdge(i,s,t){
    cy.add([{
    group: 'edges',
    data: { id: i, source: s, target:t}
  }]);
  let obj=  Object.freeze({
        id: i,
        source: s,
        target: t
      });
   arrayEdge[edgeCounter]= obj;
   edgeCounter++;
   return obj;
};

class UnionFind {
   constructor(elements) {
      // Number of disconnected components
      this.count = elements.length;
      // Keep Track of connected components
      this.parent = {};
      // Initialize the data structure such that all
      // elements have themselves as parents
      elements.forEach(e => (this.parent[e] = e));
   }
   union(a, b) {
      let rootA = this.find(a);
      let rootB = this.find(b);
      // Roots are same so these are already connected.
      if (rootA === rootB) return;
      // Always make the element with smaller root the parent.
      if (rootA < rootB) {
         if (this.parent[b] != b) this.union(this.parent[b], a);
         this.parent[b] = this.parent[a];
      } else {
         if (this.parent[a] != a) this.union(this.parent[a], b);
         this.parent[a] = this.parent[b];
      }
   }
   // Returns final parent of a node
   find(a) {
      while (this.parent[a] !== a) {
         a = this.parent[a];
      }
      return a;
   }

   // Checks connectivity of the 2 nodes
   connected(a, b) {
      return this.find(a) === this.find(b);
   }
}

// Implement a Graph
// basic operations:
//  - add vertex (node)
//  - add edge (node -> node)

function GraphNode(val) {
  this.val = val;
  this.edges = {};
}

function Graph() {
  this.vertices = {};
}

// O(1) operation
Graph.prototype.addVertex = function(val) {
  // add vertex only if it does not exist.
  if (!this.vertices[val]) {
    this.vertices[val] = new GraphNode(val);
  }
};

// O(E) operation - edges
Graph.prototype.removeVertex = function(val) {
  if (this.vertices[val]) {
    // once you remove a vertex, you need to remove all edges pointing
    // to the vertex.
    delete this.vertices[val];

    Object.keys(this.vertices).forEach(function(key, index) {
      if (this.vertices[key].edges[val]) {
        delete this.vertices[key].edges[val];
      }
    }.bind(this));
  }
};

// O(1) operation
Graph.prototype.getVertex = function(val) {
  return this.vertices[val];
};

// O(1) operation
Graph.prototype.addEdge = function(start, end) {
  // check to see if vertices exists.
  // if it exists, set the edges and be done.
  if (this.vertices[start] && this.vertices[end]) {

    // check to see if edge exists, if it does, increment it's weight
    if (this.vertices[start].edges[end]) {
      this.vertices[start].edges[end].weight += 1;
    } else {

      // edge does not exist, set weight to 1.
      this.vertices[start].edges[end] = { weight: 1 };
    }
  }
};

// O(1) operation
Graph.prototype.removeEdge = function(start, end) {
  if (this.vertices[start] && this.vertices[end]) {
    if (this.vertices[start].edges[end]) {
      delete this.vertices[start].edges[end];
    }
  }
};

// O(1) operation
Graph.prototype.getEdge = function(start, end) {
  return this.vertices[start].edges[end] || null;
};


Graph.prototype.neighbors = function(val) {
  return this.vertices[val] ? this.vertices[val].edges : null;
};
/*_____________________-FUNZIONI DA PERFEZIONARE-___________________________
document.getElementById('Cambia grafo').addEventListener("click",function(){
    cy.remove(arrayNode[0].name);
    arrayNode[0]=null;
    console.log(nodeCounter);
    nodeCounter=0;
});
document.getElementById("creaCasualGrafo").addEventListener("click", function(){
  let i= getRandomIntInclusive(1,15);
  for (let c=0; c<i;c++){
    let num=getRandomIntInclusive(1,20);
    let str= num.toString();
    creaNode(num,str);
  }
  for (let u=0;u<nodeCounter;u++){
    let id= getRandomIntInclusive(1,i);
    let s= getRandomIntInclusive(1,i);
    let t= getRandomIntInclusive(1,i);
    let cr=u++;
    creaEdge(id, arrayNode[s].name, arrayNode[t].name);
  }

});

_______________________________________________________________________________*/
/*document.getElementById('Disegna').addEventListener("click",function(){
  arrayNode[0]=creaNode(1,'1');
  arrayNode[1]=creaNode(2,'2');
  arrayNode[2]=creaNode(3,'3');
  arrayNode[3]=creaNode(4,'4');
  arrayNode[4]=creaNode(5,'5');
  arrayNode[5]=creaNode(6,'6');
  arrayNode[6]=creaNode(7,'7');
  creaEdge('10','1','6');
  creaEdge('12','3','4');
  creaEdge('14','2','7');
  creaEdge('16','2','3');
  creaEdge('18','7','4');
  creaEdge('22','5','4');
  creaEdge('24','5','7');
  creaEdge('25','6','5');
  creaEdge('28','1','2')
  console.log(edgeCounter);
  console.log(nodeCounter);
*/
function kruskalsMST() {
     // Initialize graph that'll contain the MST
     const MST = new Graph();
     this.nodes.forEach(node => MST.addNode(node));
     if (this.nodes.length === 0) {
        return MST;
     }

     // Create a Priority Queue
     edgeQueue = new PriorityQueue(this.nodes.length * this.nodes.length);

     // Add all edges to the Queue:
     for (let node in this.edges) {
        this.edges[node].forEach(edge => {
           edgeQueue.enqueue([node, edge.node], edge.weight);
        });
     }

     let uf = new UnionFind(this.nodes);

     // Loop until either we explore all nodes or queue is empty
     while (!edgeQueue.isEmpty()) {
        // Get the edge data using destructuring
        let nextEdge = edgeQueue.dequeue();
        let nodes = nextEdge.data;
        let weight = nextEdge.priority;

        if (!uf.connected(nodes[0], nodes[1])) {
           MST.addEdge(nodes[0], nodes[1], weight);
           uf.union(nodes[0], nodes[1]);
        }
     }
     return MST;
  }
};

let g = new Graph();
g.addNode("A");
g.addNode("B");
g.addNode("C");
g.addNode("D");
g.addNode("E");
g.addNode("F");
g.addNode("G");

g.addEdge("A", "C", 100);
g.addEdge("A", "B", 3);
g.addEdge("A", "D", 4);
g.addEdge("C", "D", 3);
g.addEdge("D", "E", 8);
g.addEdge("E", "F", 10);
g.addEdge("B", "G", 9);
g.addEdge("E", "G", 50);
